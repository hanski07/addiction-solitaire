﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Security.Cryptography;

namespace ExtensionMethods
{
    //Class that holds all extension methods
    public static class MyExtensions
    {
        /// <summary>
        /// Shuffle is a method in the MyExtensions class. It takes a list of type T
        /// <para> and randomizes the contents using thes Fisher and Yates method. </para> 
		/// <para> The RNGCryptoServiceProvider provides the random number.</para>
        /// <para> It is called as list.Shuffle() where list is a List of type T. </para>
        /// </summary>
        /// <typeparam name="T"> The element type of the list </typeparam>
        /// <param name="list"> Used as the object to randomize</param>
        public static void Shuffle<T>(this List<T> list)
        {
            RNGCryptoServiceProvider provider = new RNGCryptoServiceProvider();
            int n = list.Count;
            while (n > 1)
            {
                byte[] box = new byte[1];
                do provider.GetBytes(box);
				while (box[0] > Byte.MaxValue); 
                int k = (box[0] % n);
                n--;                
                T value = list[k];                         
                list[k] = list[n];
                list[n] = value;
            }

        }
    }
}
