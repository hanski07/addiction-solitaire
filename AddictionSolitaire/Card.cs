using System;
using System.Windows.Forms;
using System.Drawing;
namespace AddictionSolitaire
{
	        #region Card class
        //inherits from picture box. houses the information a card should.
        public class Card : PictureBox
        {
            char suit;
            int rank;
            int[] coords = new int[2];
            int myRow;
            int myCol;
            bool inFinalPosition = false;

            public bool isFinal
            {
                get { return inFinalPosition; }
                set { inFinalPosition = value; }
            }

            public int row
            {
                get { return myRow; }
                set { myRow = value; }
            }

            public int col
            {
                get { return myCol; }
                set { myCol = value; }
            }

            #region image array
            Bitmap[] imageArray = {Properties.Resources._01c, Properties.Resources._02c,Properties.Resources._03c,Properties.Resources._04c,Properties.Resources._05c,Properties.Resources._06c,Properties.Resources._07c,Properties.Resources._08c,Properties.Resources._09c,Properties.Resources._10c,Properties.Resources._11c,Properties.Resources._12c,Properties.Resources._13c,
                                      Properties.Resources._01d,Properties.Resources._02d,Properties.Resources._03d,Properties.Resources._04d,Properties.Resources._05d,Properties.Resources._06d,Properties.Resources._07d,Properties.Resources._08d,Properties.Resources._09d,Properties.Resources._10d,Properties.Resources._11d,Properties.Resources._12d,Properties.Resources._13d,
                                      Properties.Resources._01h,Properties.Resources._02h,Properties.Resources._03h,Properties.Resources._04h,Properties.Resources._05h,Properties.Resources._06h,Properties.Resources._07h,Properties.Resources._08h,Properties.Resources._09h,Properties.Resources._10h,Properties.Resources._11h,Properties.Resources._12h,Properties.Resources._13h,
                                      Properties.Resources._01s, Properties.Resources._02s,Properties.Resources._03s,Properties.Resources._04s,Properties.Resources._05s,Properties.Resources._06s,Properties.Resources._07s,Properties.Resources._08s,Properties.Resources._09s,Properties.Resources._10s,Properties.Resources._11s,Properties.Resources._12s,Properties.Resources._13s, };

            #endregion

            public int x
            {
                get { return coords[0]; }
                set { coords[0] = value; }
            }

            public int y
            {
                get { return coords[1]; }
                set { coords[1] = value; }
            }

            public int getRank() { return rank; }
            void setRank(int numberVal) { rank = numberVal; }
            public char getSuit() { return suit; }
            void setSuit(char suitVal) { suit = suitVal; }

            public void setCardImage(char suit, int rank)
            {
                //assigns images to card objects
                // images are 71 by 96 pixels 
                #region set suit and image
                switch (suit)
                {
                    case 'c':

                        Bitmap bmp = imageArray[rank - 1];
                        this.Image = bmp;

                        if (rank == 1) { this.Hide(); }  // if rank is an ace...

                        int x = bmp.Size.Width;
                        int y = bmp.Size.Height;
                        this.Size = new Size(x, y);
                        this.Name = suit.ToString() + rank.ToString();

                        break;

                    case 'd':
                        Bitmap bmp1 = imageArray[rank - 1 + 13];
                        this.Image = bmp1;
                        if (rank == 1) { this.Hide(); }
                        int x1 = bmp1.Size.Width;
                        int y1 = bmp1.Size.Height;
                        this.Size = new Size(x1, y1);
                        this.Name = suit.ToString() + rank.ToString();

                        break;

                    case 'h':

                        Bitmap bmp2 = imageArray[rank - 1 + 26];
                        this.Image = bmp2;
                        if (rank == 1) { this.Hide(); }
                        int x2 = bmp2.Size.Width;
                        int y2 = bmp2.Size.Height;
                        this.Size = new Size(x2, y2);
                        this.Name = suit.ToString() + rank.ToString();

                        break;

                    case 's':
                        Bitmap bmp3 = imageArray[rank - 1 + 39];
                        this.Image = bmp3;
                        if (rank == 1) { this.Hide(); }
                        int x3 = bmp3.Size.Width;
                        int y3 = bmp3.Size.Height;
                        this.Size = new Size(x3, y3);
                        this.Name = suit.ToString() + rank.ToString();

                        break;




                }

                #endregion
            }

            public Card(char suitVal, int numberVal)
            {
                setRank(numberVal);
                setSuit(suitVal);
                setCardImage(suit, rank);
            }
        }
        #endregion

    }



