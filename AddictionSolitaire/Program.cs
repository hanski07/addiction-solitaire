﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using System.Drawing;
using System.IO;
using System.Drawing.Imaging;
using System.Resources;
using System.Collections;
using ExtensionMethods;

namespace AddictionSolitaire
{


    #region Main Form
    class SolitaireForm : Form
    {
        const char c = 'c';
        const char d = 'd';
        const char h = 'h';
        const char s = 's';
        bool mouseDown;
        bool doubleClicked;
        int count;
        bool gameOver;
        int scoreCount = 0;
        MainMenu mainMenu1;
        MenuItem menuItem1; //File
        MenuItem menuItem2; //New
        MenuItem menuItem3; //Quit
        MenuItem menuItem4; // For my Fianceé (held Alt and typed 130 on the numpad to get the accent).
        MenuItem menuItem5; // click me :)

        List<List<Card>> cardList = new List<List<Card>>();       //master list that is the underlying structure of the game.
        bool newGamePressed = false;                              // allows us to not add multiple button objects to the screen.
        Button reshuffle = new Button();
        Label scoreBox;
		Label scoreTitle;

        int redrawCount = 0;

        public int reshuffleCount
        {
            get { return count; }
            set { count = value; }
        }

        public SolitaireForm()
        {
            this.BackColor = Color.Green;
            initializeComponents();//Card objects are created,shuffled and stored in a master list of lists                                                                                           
        }

        public void initializeComponents()
        {
			if (!newGamePressed)
            {
	            #region create menus

	            this.mainMenu1 = new MainMenu();
	            this.menuItem1 = new MenuItem("File");
	            this.menuItem2 = new MenuItem("New");
	            this.menuItem3 = new MenuItem("Quit");
	            this.menuItem4 = new MenuItem("For my fianceé");
	            this.menuItem5 = new MenuItem("Click me :) ");

	            //add menu items to main menu object
	            this.mainMenu1.MenuItems.AddRange(new MenuItem[] {
	                this.menuItem1,
	                this.menuItem4});

	            //add menu items below file.
	            this.menuItem1.MenuItems.Add(this.menuItem2);
	            this.menuItem1.MenuItems.Add(this.menuItem3);

	            this.menuItem4.MenuItems.Add(this.menuItem5);

	            //set menu on form
	            this.Menu = this.mainMenu1;

	            //define delegate and assign it a method
	            menuItem2.Click += new EventHandler(newGameButton_Click);
	            menuItem3.Click += new EventHandler(quit_Click);
	            menuItem5.Click += new EventHandler(menuItem5_Click);


	            #endregion

	            #region createButtons&ScoreBox
            
                Button newGameButton = new Button();
                newGameButton.BackColor = Color.Beige;
                newGameButton.Location = new Point(1120, 50);
                newGameButton.Text = "New Game";
                this.Controls.Add(newGameButton);


                reshuffle.BackColor = Color.Beige;
                reshuffle.Location = new Point(1120, 100);
                reshuffle.Text = "Reshuffle";
                this.Controls.Add(reshuffle);



                Button quit = new Button();
                quit.BackColor = Color.Beige;
                quit.Location = new Point(1120, 150);
                quit.Text = "Quit";
                this.Controls.Add(quit);

                newGameButton.Click += new EventHandler(newGameButton_Click);
                reshuffle.Click += new EventHandler(reshuffle_Click);
                quit.Click += new EventHandler(quit_Click);

				scoreTitle = new Label();
				scoreTitle.Location = new Point(1120,200);
				scoreTitle.Text = ("Score");
				scoreTitle.Font = new Font ("Arial", 12);
				scoreTitle.ForeColor = Color.Black;
				scoreTitle.AutoSize = true;
				this.Controls.Add (scoreTitle);
				scoreBox = new Label();
	            scoreBox.ForeColor = Color.Black;
	            scoreBox.Location = new Point(1120, 220);
	            scoreBox.Text = scoreCount.ToString();
	            scoreBox.Font = new Font("Arial", 17);
	            scoreBox.AutoSize = true;
				this.Controls.Add(scoreBox);
            }

            #endregion
            List<Card> subList = new List<Card>();
            fillCardList(cardList, subList);

            //call 'unnestAndShuffle' to ease the pain of randomizing the nested list...
            //the shuffle method is called within 
            unnestAndShuffle(cardList);
            // place shuffled cards into positions on the game board and
            // set the row and col for the card object
            reDraw();
            updateIsFinal();
            setScore();

        }

        private void unnestAndShuffle(List<List<Card>> list)
        {
            List<Card> tempList = new List<Card>();
            List<Card> subList = new List<Card>();

            List<Card> finalList = new List<Card>(); // list to keep cards that are "final" from reshuffling.


            foreach (List<Card> item in list)
            {
                foreach (Card card in item)
                {
                    //add final cards to the final list. to be inserted back into the reshuffled tempList later.
                    if ((card.isFinal) && !(finalList.Contains(card))) { finalList.Add(card); }

                    else { tempList.Add(card); }
                }

            }

            tempList.Shuffle();
            list.Clear();
            
            //insert cards into correct position.
            for (int i = 0; i < finalList.Count; i++)
            {
                Card tempCard = finalList[i];
                tempList.Insert((tempCard.row * 13) + (tempCard.col), tempCard);

            }

            int rowCount = 0;
            for (int j = 0; j < 52; j++)
            {
                subList.Add(tempList[j]);
                if (subList.Count == 13)
                {
                    rowCount++;
                    list.Add(subList.ToList());
                    subList.Clear();
                }
            }
        }

        private void fillCardList(List<List<Card>> cardList, List<Card> subList)
        {
            //fill the nested card list used as the underlying data structure for the game
            //also assignes mouse event delegates to every card object

            #region initialize cards into nested list
            for (int i = 0; i < 52; i++)
            {
                if (i <= 12)
                {
                    Card card = new Card(c, (i + 1));
                    //relate delegates to call MouseDown, MouseUp
                    card.MouseDown += new MouseEventHandler(OnMouseDown);
                    card.MouseMove += new MouseEventHandler(OnMouseMove);
                    card.MouseUp += new MouseEventHandler(OnMouseUp);
                    card.MouseDoubleClick += new MouseEventHandler(OnMouseDoubleClick);
                    subList.Add(card);

                    if (i == 12) { cardList.Add(subList.ToList()); subList.Clear(); }
                }

                else if ((i > 12) && (i <= 25))
                {
                    Card card = new Card(d, (i + 1) - 13);
                    card.MouseDown += new MouseEventHandler(OnMouseDown);
                    card.MouseMove += new MouseEventHandler(OnMouseMove);
                    card.MouseUp += new MouseEventHandler(OnMouseUp);
                    card.MouseDoubleClick += new MouseEventHandler(OnMouseDoubleClick);
                    subList.Add(card);

                    if (i == 25) { cardList.Add(subList.ToList()); subList.Clear(); }

                }

                else if ((i > 25) && (i <= 38))
                {
                    Card card = new Card(h, (i + 1) - 26);
                    card.MouseDown += new MouseEventHandler(OnMouseDown);
                    card.MouseMove += new MouseEventHandler(OnMouseMove);
                    card.MouseUp += new MouseEventHandler(OnMouseUp);
                    card.MouseDoubleClick += new MouseEventHandler(OnMouseDoubleClick);
                    subList.Add(card);

                    if (i == 38) { cardList.Add(subList.ToList()); subList.Clear(); }

                }

                else if (i > 38)
                {

                    Card card = new Card((char)s, (i + 1) - 39);
                    card.MouseDown += new MouseEventHandler(OnMouseDown);
                    card.MouseMove += new MouseEventHandler(OnMouseMove);
                    card.MouseUp += new MouseEventHandler(OnMouseUp);
                    card.MouseDoubleClick += new MouseEventHandler(OnMouseDoubleClick);
                    subList.Add(card);

                    if (i == 51)
                    {
                        cardList.Add(subList.ToList()); subList.Clear();
                    }
                }
                else
                {
                    throw new SystemException();
                }

            }
            #endregion
        }

        public void reDraw()
        {
            for (int row = 0; row < 4; row++)
            {
                for (int col = 0; col < 13; col++)
                {
                    Card tempCard = cardList[row][col];


                    double gap = 1.2;

                    int x = (int)((tempCard.Width * col) * (gap)); // width is 71 using the standard deck
                    int y = (int)((tempCard.Height * row) * (gap)) + 60; // height is 96

                    tempCard.col = col;
                    tempCard.row = row;

                    tempCard.x = x;
                    tempCard.y = y;

                    tempCard.Location = new Point(x, y);
                    if (redrawCount == 0) { this.Controls.Add(tempCard); }

                }

            }
        }

        static class Initialize
        {

            #region main function
            static void Main()
            {
                Form mainForm = new SolitaireForm();
                mainForm.Size = new Size(1220, 600);
                mainForm.Text = "Addiction Solitaire!";

                Application.Run(mainForm);


            }
        }
            #endregion
        //---------------------------------------------------------------------------------------------//
        //Event handling methods

        //OnMouseUp and OnMouseDown simulate a 'drag and drop' interface by storing the coords of
        //the card to be moved and storing the coords of the card they intend to replace.
        #region MouseEvents

        private void OnMouseDown(object sender, MouseEventArgs e)
        {
            doubleClicked = false;
            if (e.Button == MouseButtons.Left)
            {
                mouseDown = true;
                Card thisCard = (Card)sender;
                thisCard.x = e.X; //"old" x point of mouse
                thisCard.y = e.Y; // "old" y point of mouse
            }
        }

        private void OnMouseDoubleClick (object sender, MouseEventArgs e)
		{

			mouseDown = false;
			doubleClicked = true;
			Card clickedCard = (Card)sender;
			Card movingToCard = null;

			foreach (List<Card> row in cardList) {
				foreach (Card cardBefore in row) {

					//find the card one rank below of the same suit
					//check if the next card is an ace
					//switch cards.
					if (clickedCard.getRank () > 2 && (cardBefore.getSuit () == clickedCard.getSuit ()) && (cardBefore.getRank () == clickedCard.getRank () - 1)) {
						movingToCard = cardList [cardBefore.row] [cardBefore.col + 1];

					}

				}
			}
          

			updateGameComponents (movingToCard, clickedCard);

			updateIsFinal ();
			setScore ();

			if ( isGameWon() ) { gameWon (); }
					
        }


        private void OnMouseMove(object sender, MouseEventArgs e)
        {
            if (mouseDown && !doubleClicked)
            {
                Card clickedCard = (Card)sender;
                clickedCard.BringToFront();

                clickedCard.Location = new Point(clickedCard.Location.X - (clickedCard.x - e.X), // subtract the difference between the 'old' point of the mouse  
                                                clickedCard.Location.Y - (clickedCard.x - e.Y)); // and the current point of the mouse from the card's current location.
            }
        }

        
        private void OnMouseUp(object sender, MouseEventArgs e)
        {

            if (!doubleClicked)
            {
                //cardToBeMoved is the card currently in the potential position where we let up the mouse click.
                mouseDown = false;
                double gap = 1.2;
				Card clickedCard = (Card)sender;
                int newRow = (int)(clickedCard.Location.Y / (clickedCard.Height * gap));
                int newCol = (int)Math.Round((clickedCard.Location.X / (clickedCard.Width * gap))); //use the round, otherwise the card will switch with
                																					// an unexpected card (the card one column to the left).
                if ((newRow < 4 && newRow >= 0) && (newCol < 14 && newCol >= 0))
                {
                    Card movingToCard = cardList[newRow][newCol];

                    #region game logic

                    if (movingToCard.getRank() == 1 && (movingToCard.col > 0)) // prevents out of range errors and moving cards to non blank spaces.
                    {
                        Card neighborCard = cardList[movingToCard.row][movingToCard.col - 1];


                        //checks to see if the ranks of the neighbor and clicked are the same, and if the rank of the clicked is one greater of the neighbor.
                        // also checks to see if the neighbor is a king; if it is, no go.
                        if ((clickedCard.getRank() == neighborCard.getRank() + 1) && (clickedCard.getSuit() == neighborCard.getSuit())
                             && (neighborCard.getRank() < 13))
                        {
							updateGameComponents(movingToCard, clickedCard);
                        }

                        else //card was placed on an ace, but neighbor was not one below it, and/or of same suit. also handles the king move (i.e., placed in front of a king == no).
                        {
                            int originalX = (int)(clickedCard.Width * clickedCard.col * gap);
                            int originalY = (int)(clickedCard.Height * clickedCard.row * gap) + 60;

                            clickedCard.Location = new Point(originalX, originalY);
                        }
                    }


                    //check to see if we're moving a 2 to the first column. allow this.
                    else if (clickedCard.getRank() == 2 && movingToCard.getRank() == 1 && movingToCard.col == 0)
                    {
						updateGameComponents(movingToCard, clickedCard);

                    }

                    else //handles cards placed on other cards where (i.e., a non-blank space).
                    {
                        int originalX = (int)(clickedCard.Width * clickedCard.col * gap);
                        int originalY = (int)(clickedCard.Height * clickedCard.row * gap) + 60;

                        clickedCard.Location = new Point(originalX, originalY);
                    }

                }

                else //takes care of cards clicked too far to the left.
                {
                    int originalX = (int)(clickedCard.Width * clickedCard.col * gap);
                    int originalY = (int)(clickedCard.Height * clickedCard.row * gap) + 60;

                    clickedCard.Location = new Point(originalX, originalY);

                }

                //used to determine what cards to leave alone during the reshuffle.
                updateIsFinal();

                //check to see if they've won!
				if (isGameWon ()) { gameWon(); }
            }

            setScore();
        }

                    #endregion

        #endregion

        #region Button Events

        void newGameButton_Click(object sender, EventArgs e)
        {
            #region reset components
            this.cardList.Clear();
            controlClear(this.Controls);
            reshuffleCount = 0;
            redrawCount = 0;
            this.newGamePressed = true;
            this.reshuffle.Enabled = true;
            this.gameOver = true;
            scoreCount = 0;
            #endregion

            initializeComponents();

        }

        void reshuffle_Click (object sender, EventArgs e)
		{
			redrawCount++;
			reshuffleCount++;

			updateIsFinal();
			unnestAndShuffle (cardList);
			reDraw ();
			updateIsFinal();
			setScore ();

			// add check to see if we've reshuffled too many times (3)
			if (reshuffleCount >= 3) {
				Button thisButton = (Button)sender;
				thisButton.Enabled = false;
			}

			//check to see if they've won! (you never know...more for debugging purposes, but hey!...)
			if (isGameWon ()) {
				gameWon();
			}
        }

        void quit_Click(object sender, EventArgs e)
        {

            this.Close();

        }

        #endregion

		#region hepler methods

        void menuItem5_Click(object sender, EventArgs e)
        {

            MessageBox.Show("I love you so much!!!!! You're the best! :D ");

        }
        void controlClear(Control.ControlCollection cc)
        {

            for (int i = cc.Count - 1; i >= 0; i--)
            {
                if (cc[i] as Button == null && cc[i] as Label == null) { cc.Remove(cc[i]); } //leave the buttons and scorebox on the screen. 

            }
        }

        void gameWon() { MessageBox.Show("Congrats love!!!! :) You did it!"); }

        void setScore()
        {
            scoreCount = 0;
            foreach (List<Card> row in cardList)
            {
                foreach (Card card in row)
                {

                    if (card.isFinal) { scoreCount += 1000; }

                }
            }
			scoreBox.Text = scoreCount.ToString();
        }

        void updateIsFinal()
        {
			//update isFinal tags; used to determine if a card is in the correct position. If so, leave alone.
            bool rowDone;

            foreach (List<Card> row in cardList)
            {
                rowDone = false;

                foreach (Card card in row)
                {

                    if ((card.col == 0) && (card.getRank() == 2)) { card.isFinal = true; }
                    else if (!rowDone && (card.col > 0) && (card.getRank() == cardList[card.row][card.col - 1].getRank() + 1)
                        && card.getSuit() == cardList[card.row][card.col - 1].getSuit()) { card.isFinal = true; }
                    else { card.isFinal = false; rowDone = true; }

                }

            }

        }

		void updateGameComponents (Card movingToCard, Card clickedCard)
		{

			if (movingToCard != null && movingToCard.getRank () == 1) {
				double gap = 1.2;
				int clickedX = (int)(clickedCard.Width * clickedCard.col * gap);
				int clickedY = (int)(clickedCard.Height * clickedCard.row * gap) + 60;

				int clickedRow = clickedCard.row;
				int clickedCol = clickedCard.col;

				updateGameComponentsHelper(clickedCard, movingToCard, clickedX, clickedY, clickedRow, clickedCol);

			}
		}

		void updateGameComponentsHelper(Card clickedCard, Card movingToCard, int clickedX, int clickedY, int clickedRow, int clickedCol)
			{
				//update gui
				clickedCard.Location = new Point (movingToCard.Location.X, movingToCard.Location.Y);
				movingToCard.Location = new Point (clickedX, clickedY);

				//update cardList
				cardList [movingToCard.row] [movingToCard.col] = clickedCard;
				cardList [clickedRow] [clickedCol] = movingToCard;

				//update row and col
				clickedCard.row = movingToCard.row;
				clickedCard.col = movingToCard.col;

				movingToCard.row = clickedRow;
				movingToCard.col = clickedCol;

			}

		bool isGameWon ()
		{
			if (cardList[0][11].isFinal == true && cardList[1][11].isFinal == true && cardList[2][11].isFinal == true && cardList[3][11].isFinal == true && !gameOver){ 

				return gameOver = true; 
			}
			return gameOver = false;
		}

		#endregion
    #endregion

	}
}